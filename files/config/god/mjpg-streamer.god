God.watch do |w|
  w.name = "mjpg-streamer"
  w.start = "/usr/local/bin/mjpg_streamer --input '/usr/local/lib/mjpg-streamer/input_uvc.so --device /dev/video0 --resolution 1280x720 --fps 5' --output '/usr/local/lib/mjpg-streamer/output_http.so'"
  w.keepalive(:memory_max => 150.megabytes)
  w.uid = "camera"
  w.gid = "video"
end
