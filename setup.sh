#!/bin/bash

apt-get update;
apt-get -y dist-upgrade;
apt-get -y install vim libjpeg8 libav-tools v4l-utils ruby ruby-god \
                   raspi-config rpi-update;

# Run Raspberry Pi updater.
rpi-update;

# Anything below this line can run without interactivity

# Clean up after update.
if [ -d /boot.bak ];
then
  rm -rf /boot.bak
fi

if [ -d /lib/modules.bak ];
then
  rm -rf /lib/modules.bak;
fi

# Create the camera user.
/usr/bin/id camera > /dev/null 2>&1;

if [ x"${?}" = x"1" ];
then
  useradd --no-create-home --comment "Camera User" -G video camera;
fi

# Update config.txt
cp /boot/config.txt /boot/config.bak;
echo "core_freq=250" >> /boot/config.txt
echo "sdram_freq=400" >> /boot/config.txt
echo "over_voltage=0" >> /boot/config.txt

# Install MJPG streamer
if [ ! -d /usr/local/lib/mjpg-streamer ];
then
  mkdir --parents /usr/local/lib/mjpg-streamer;
fi;

if [ ! -d /usr/local/bin ];
then
  mkdir --parents /usr/local/bin;
fi;

# Copy files into the setup directories.
cp -rf files/mjpg-streamer/plugins/input_testpicture.so \
       /usr/local/lib/mjpg-streamer;
cp -rf files/mjpg-streamer/plugins/output_file.so \
       /usr/local/lib/mjpg-streamer;
cp -rf files/mjpg-streamer/plugins/input_uvc.so \
       /usr/local/lib/mjpg-streamer;
cp -rf files/mjpg-streamer/plugins/output_udp.so \
       /usr/local/lib/mjpg-streamer;
cp -rf files/mjpg-streamer/plugins/input_file.so \
       /usr/local/lib/mjpg-streamer;
cp -rf files/mjpg-streamer/plugins/output_http.so \
       /usr/local/lib/mjpg-streamer;
cp -rf files/mjpg-streamer/bin/mjpg_streamer \
       /usr/local/bin;

# Configure God
if [ ! -d /etc/god.d ];
then
  mkdir --parents /etc/god.d;
  chown root:root /etc/god.d;
  chmod 0750      /etc/god.d;
fi

cp -rf files/config/god/god.init /etc/init.d/god;
chown root:root /etc/init.d/god;
chmod 0755 /etc/init.d/god;
update-rc.d -f god defaults;

cp -rf files/config/god/god.conf /etc/god.conf;
chown root:root /etc/god.conf;
chmod 0600      /etc/god.conf;

cp -rf files/config/god/mjpg-streamer.god /etc/god.d/mjpg-streamer.god;
chown root:root /etc/god.d/mjpg-streamer.god;
chmod 0600      /etc/god.d/mjpg-streamer.god;

# Stop uneccessary services.
update-rc.d -f rpcbind    remove;
update-rc.d -f nfs-common remove;
update-rc.d -f exim4      remove;
update-rc.d -f rsync      remove;
update-rc.d -f plymouth   remove;
