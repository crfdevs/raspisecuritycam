# Raspberry Pi Security Camera Setup

## Installation
Clone this repo on the PI and run setup.sh. It will install the camera modules checked in to the files/ folder.

    sudo ./setup.sh
    
## Building
If you want to build the camera modules on the PI. These are checked in to the files/ folder.

    sudo ./build.sh