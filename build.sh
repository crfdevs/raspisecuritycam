#!/bin/bash

apt-get update;
apt-get -y dist-upgrade;
apt-get -y install vim build-essential subversion libjpeg8-dev imagemagick \
                   libav-tools v4l-utils ruby ruby-god raspi-config rpi-update;

if [ ! -d tmp ];
then
  mkdir --parents tmp;
fi

if [ ! -d tmp/mjpg-streamer/.svn ];
then
  svn checkout svn://svn.code.sf.net/p/mjpg-streamer/code/mjpg-streamer/ tmp/mjpg-streamer/
else
  ( cd tmp/mjpg-streamer && svn update );
fi

# Build MJPG Streamer
( cd tmp/mjpg-streamer && make );

# Copy files into the setup directories.
cp -rf tmp/mjpg-streamer/input_testpicture.so files/mjpg-streamer/plugins/;
cp -rf tmp/mjpg-streamer/output_file.so       files/mjpg-streamer/plugins/;
cp -rf tmp/mjpg-streamer/input_uvc.so         files/mjpg-streamer/plugins/;
cp -rf tmp/mjpg-streamer/output_udp.so        files/mjpg-streamer/plugins/;
cp -rf tmp/mjpg-streamer/input_file.so        files/mjpg-streamer/plugins/;
cp -rf tmp/mjpg-streamer/output_http.so       files/mjpg-streamer/plugins/;
cp -rf tmp/mjpg-streamer/mjpg_streamer        files/mjpg-streamer/bin/;
